package com.jouranldev.struts2.dao;

import java.util.List;

import com.journaldev.struts2.domain.Employee;

public interface EmployeeDAO {

	public void saveOrUpdateUser(Employee user);
	public List<Employee> listUser();
	public Employee listUserById(Long userId);
	public void deleteUser(Long userId);
}
