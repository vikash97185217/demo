package com.journaldev.struts2.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EMPLOYEE")
public class Employee {

	private Long id;
	private String name;
	private String dept;
	private String age;
	
	@Id
	@GeneratedValue
	@Column(name="ID")	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="DEPT")
	public String getDept() {
		return dept;
	}
	public void setGender(String dept) {
		this.dept = dept;
	}
	
	@Column(name="AGE")
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}

}
