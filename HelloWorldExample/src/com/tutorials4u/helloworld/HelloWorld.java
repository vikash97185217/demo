package com.tutorials4u.helloworld;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionSupport;



public class HelloWorld extends ActionSupport{

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(HelloWorld.class);

	private String message;

    private String userName;
    
    public HelloWorld() {
    }

    public String execute()  {
    	logger.info("Starting of execute in HelloWorld");
        setMessage("Hello " + getUserName());
        logger.info("Ending of execute in HelloWorld");
        return "SUCCESS";
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

}